package aho.gms;

import java.util.concurrent.atomic.AtomicLong;

public class Client {
    public final String ip;
    public final AtomicLong lastSeen = new AtomicLong(System.currentTimeMillis());

    public Client(final String ip) {
	this.ip = ip;
    }

    @Override
    public String toString() {
	return ip;
    }

    public void disconnect() {
	Client client = null;
	for (final Client c : Vars.listClients)
	    if (c.ip.equals(this.ip))
		client = c;
	if (client == null)
	    return;
	Vars.listClients.wait4Unlock();
	Vars.listClients.setLocked(true);
	Vars.listClients.remove(client);
	Vars.listClients.setLocked(false);
    }
    
    public void updateLastSeen(){
	for (int i = 0; i < Vars.listClients.size(); i++)
	    if (Vars.listClients.get(i).ip.contentEquals(this.ip))
		Vars.listClients.get(i).lastSeen.set(System.currentTimeMillis());
    }

    public boolean exists() {
	for (final Client c : Vars.listClients)
	    if (c.ip.equals(this.ip))
		return true;
	return false;
    }
}
