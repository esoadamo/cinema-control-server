package aho.gms;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

@SuppressWarnings("serial")
public class FrameCinema extends JFrame {

    private JPanel contentPane;
    public JLabel lbl;

    /**
     * Create the frame.
     */
    public FrameCinema() {
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 1366, 768);
	setExtendedState(JFrame.MAXIMIZED_BOTH);
	setUndecorated(true);
	if (Vars.isUnixLike())
	    setAlwaysOnTop(true);
	contentPane = new JPanel();
	contentPane.setBackground(Color.BLACK);
	contentPane.setLayout(new BorderLayout(0, 0));
	setIconImage(new ImageIcon(Vars.class.getResource("/icon.png")).getImage());
	setContentPane(contentPane);

	lbl = new JLabel("Welcome to the cinema");
	lbl.setFont(new Font("Tahoma", Font.BOLD, this.getHeight() / 20));
	lbl.setForeground(Color.WHITE);
	lbl.setHorizontalAlignment(JLabel.CENTER);
	contentPane.add(lbl, BorderLayout.CENTER);

	final BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
	final Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
	getContentPane().setCursor(blankCursor);
    }

    public void updateStatusText() throws SocketException {
	if (Vars.videoPlayerProcess != null && Vars.videoPlayerProcess.isAlive()) {
	    lbl.setText("");
	    return;
	}
	final StringBuilder sb = new StringBuilder();
	sb.append("<html>");
	sb.append("Welcome to the cinema<br>");
	Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
	for (; n.hasMoreElements();) {
	    NetworkInterface e = n.nextElement();

	    Enumeration<InetAddress> a = e.getInetAddresses();
	    while (a.hasMoreElements()) {
		InetAddress addr = a.nextElement();
		sb.append(addr.getHostAddress());
		sb.append(':');
		sb.append(Vars.serverPort);
		sb.append("<BR>");
	    }
	}
	sb.append("Clients connected: ");
	sb.append(Vars.listClients.size());
	sb.append("</html>");
	lbl.setText(sb.toString());
    }

}
