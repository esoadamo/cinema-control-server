package aho.gms.maps;

import java.awt.Robot;
import java.awt.event.KeyEvent;

public class Totem implements KeyMap {

    @Override
    public void pause(Robot r) {
	type(KeyEvent.VK_SPACE, r);
    }

    @Override
    public void forward(Robot r) {
	type(KeyEvent.VK_RIGHT, r);
    }

    @Override
    public void forward2(Robot r) {
	type(KeyEvent.VK_RIGHT, r);
    }

    @Override
    public void backward(Robot r) {
	type(KeyEvent.VK_LEFT, r);
    }

    @Override
    public void backward2(Robot r) {
	type(KeyEvent.VK_LEFT, r);
    }

    @Override
    public void volumeUp(Robot r) {
	type(KeyEvent.VK_UP, r);
    }

    @Override
    public void volumeDown(Robot r) {
	type(KeyEvent.VK_DOWN, r);
    }

    @Override
    public void subtitlesSwitch(Robot r) {
	//Not implemented in Totem
    }

    @Override
    public void nextSubtitles(Robot r) {
	//Not implemented in Totem

    }

    @Override
    public void prevSubtitles(Robot r) {
	//Not implemented in Totem

    }

    @Override
    public void nextAudio(Robot r) {
	//Not implemented in Totem

    }

    @Override
    public void prevAudio(Robot r) {
	//Not implemented in Totem
    }

    @Override
    public void speedUp(Robot r) {
	//Not implemented in Totem
    }

    @Override
    public void speedDown(Robot r) {
	//Not implemented in Totem

    }

    private static void type(final int key, final Robot robot) {
	robot.keyPress(key);
	try {
	    Thread.sleep(100);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	robot.keyRelease(key);
    }

    @Override
    public void toggleFullscreen(Robot r) {
	type(KeyEvent.VK_F, r);
    }

    @Override
    public void adjustVideoFormat(Robot r) {
	type(KeyEvent.VK_A, r);
    }

}
