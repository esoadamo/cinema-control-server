package aho.gms;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletRequest;

/**
 * Basic properties in style KEY=VALUE&KEY2=VALUE2
 */
public class DSMap {
    private final String[] keys;
    private final String[] values;
    public static final String separator = "&";

    public DSMap(final String data) {
	final List<String> keys = new ArrayList<String>();
	final List<String> values = new ArrayList<String>();
	for (final String upperLevel : data.split(separator)) {
	    final String[] parts = upperLevel.split("=");
	    keys.add(parts[0]);
	    values.add(parts[1]);
	}
	this.keys = keys.toArray(new String[keys.size()]);
	this.values = values.toArray(new String[values.size()]);
    }

    public DSMap(final ServletRequest request) {
	final List<String> keys = new ArrayList<String>();
	final List<String> values = new ArrayList<String>();
	for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
	    final String key = e.nextElement();
	    keys.add(key);
	    values.add(request.getParameter(key));
	}
	this.keys = keys.toArray(new String[keys.size()]);
	this.values = values.toArray(new String[values.size()]);
    }

    /**
     * Finds key and assigns value to it
     * 
     * @param key
     *            key of the value
     * @return value if found, null otherwise
     */
    public String getString(String key) {
	for (int i = 0; i < keys.length && i < values.length; i++)
	    if (keys[i].contentEquals(key))
		return values[i];
	return null;
    }

    /**
     * Finds key and assigns value to it
     * 
     * @param key
     *            key of the value
     * @return value if found, null otherwise
     */
    public Integer getInteger(String key) {
	final String s = getString(key);
	if (s == null)
	    return null;
	return Integer.parseInt(s);
    }

    /**
     * Finds key and assigns value to it
     * 
     * @param key
     *            key of the value
     * @return value if found, null otherwise
     */
    public Double getDouble(String key) {
	final String s = getString(key);
	if (s == null)
	    return null;
	return Double.parseDouble(s);
    }

    /**
     * Checks if this key exists
     * 
     * @param key
     * @return
     */
    public boolean keyExists(final String key) {
	for (int i = 0; i < keys.length; i++)
	    if (keys[i].contentEquals(key))
		return true;
	return false;
    }

    /**
     * List all keys inside this dictionary map
     * 
     * @return all keys inside this dictionary map
     */
    public String[] getKeys() {
	return keys;
    }

    /**
     * Lists all keys and values, returns data in format KEY1=VALUE1&KEY2=VALUE2
     * 
     * @return keys and values in format KEY1=VALUE1&KEY2=VALUE2
     */
    public String getFormatedInputString() {
	return getFormatedInputString(null);
    }

    /**
     * Lists all keys and values, returns data in format KEY1=VALUE1&KEY2=VALUE2
     * 
     * @param excludedKeys
     *            array of keys ment to be excluded
     * @return keys and values in format KEY1=VALUE1&KEY2=VALUE2
     */
    public String getFormatedInputString(final String[] excludedKeys) {
	final StringBuilder sb = new StringBuilder();
	for (final String s : getKeysWithValues(excludedKeys)) {
	    sb.append(s);
	    sb.append(separator);
	}
	return sb.toString();
    }

    /**
     * Lists all keys and values, returns data in format KEY1=VALUE1
     * 
     * @return keys and values in format KEY1=VALUE1
     */
    public String[] getKeysWithValues() {
	return getKeysWithValues(null);
    }

    /**
     * Lists all keys and values, returns data in format KEY1=VALUE1
     * 
     * @param excludedKeys
     *            array of keys ment to be excluded
     * @return keys and values in format KEY1=VALUE1
     */
    public String[] getKeysWithValues(final String[] excludedKeys) {
	final String[] returnArray = new String[keys.length];
	final StringBuilder sb = new StringBuilder();
	for (int i = 0; i < keys.length; i++) {
	    if (excludedKeys != null) {
		boolean excluded = false;
		for (final String excludedKey : excludedKeys)
		    if (excludedKey.contentEquals(keys[i])) {
			excluded = true;
			break;
		    }
		if (excluded)
		    continue;
	    }
	    sb.setLength(0);
	    sb.append(keys[i]);
	    if (i < values.length) {
		sb.append("=");
		sb.append(values[i]);
	    }
	    returnArray[i] = sb.toString();
	}
	return returnArray;
    }
}
