
package aho.gms;

import java.io.File;
import java.net.SocketException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.jetty.server.Server;

import aho.gms.maps.VLC;
import aho.util.LEArrayList;
import aho.util.ProgramArguments;

public class CinemaServer {

    public static void main(String[] args) {
	ProgramArguments pargs = new ProgramArguments(args);
	if (pargs.argExist("?") || pargs.argExist("h") || pargs.argExist("help") || pargs.argExist("--help")) {
	    System.out.println("-vlcPath ... sets vlc exec path");
	    System.out.println("-videoPath ... sets video root");
	    System.exit(0);
	}

	if (pargs.argExist("vlcPath")) {
	    Vars.controls = new VLC();
	    VideoPlayer.winVLCPath = pargs.getArgValue("vlcPath");
	}

	if (pargs.argExist("videoPath")) {
	    Vars.dirCinema = new File(pargs.getArgValue("videoPath"));
	}

	System.out.printf("Hello!%nYou are running '%s' server, version %f%n", Vars.gameName, Vars.version);
	Vars.frame.setVisible(true);

	System.out.println("Starting server...");
	Vars.frame.lbl.setText("Starting server...");
	Vars.server = new Server(Vars.serverPort);
	Vars.server.setHandler(new HTTPHandler());
	try {
	    Vars.server.start();
	    Vars.frame.updateStatusText();

	    new Timer().schedule(new TimerTask() {

		@Override
		public void run() {
		    final LEArrayList<Client> timedOutClients = new LEArrayList<Client>();
		    for (final Client c : Vars.listClients)
			if (System.currentTimeMillis() - c.lastSeen.get() > 15000) {
			    timedOutClients.add(c);
			    System.out.printf("%s timed out%n", c.toString());
			}

		    Vars.listClients.wait4Unlock();
		    Vars.listClients.setLocked(true);
		    for (final Client c : timedOutClients)
			Vars.listClients.remove(c);
		    Vars.listClients.setLocked(false);
		    try {
			Vars.frame.updateStatusText();
		    } catch (SocketException e) {
			e.printStackTrace();
		    }
		}

	    }, 5000, 5000);
	} catch (Exception e) {
	    Vars.frame.lbl.setText("<html>Starting server failed: " + e.getMessage().replaceAll("\n", "<br>") + "</html>");
	    e.printStackTrace();
	}

	final Scanner sc = new Scanner(System.in);

	while (true) {
	    final String line = sc.nextLine();
	    if (line.equalsIgnoreCase("quit") || line.equalsIgnoreCase("exit")) {
		Vars.disconnectClients.set(true);
		try {
		    Thread.sleep(5000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		sc.close();
		System.out.println("Bye");
		System.exit(0);
	    }
	}
    }
}
