package aho.gms;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;

public class VideoPlayer {
    public static String currentFilename = "nothing";
    public static Robot robot;

    static {
	try {
	    robot = new Robot();
	} catch (AWTException e) {
	    e.printStackTrace();
	}
    }

    public static void kill() throws IOException {
	if (Vars.videoPlayerProcess != null && Vars.videoPlayerProcess.isAlive()) {
	    Vars.videoPlayerProcess.destroy();
	}
    }

    public static String winVLCPath = null;

    public static void playFile(final File f) throws IOException {
	kill();
	currentFilename = f.getName();
	ProcessBuilder b = null;
	if (Vars.isUnixLike())
	    b = new ProcessBuilder("xterm", "-e", "omxplayer", "-o", "local", f.getAbsolutePath());
	else {
	    if (winVLCPath == null) {
		if (System.getenv("programfiles(x86)").length() > 1)
		    winVLCPath = System.getenv("programfiles(x86)") + "\\VideoLAN\\VLC\\vlc.exe";
		else
		    winVLCPath = System.getenv("programfiles") + "\\VideoLAN\\VLC\\vlc.exe";
	    }
	    b = new ProcessBuilder(winVLCPath, "--fullscreen", f.getAbsolutePath());
	}
	Vars.videoPlayerProcess = b.start();
	Vars.frame.updateStatusText();
	currentFilename = f.getName();
	new Thread() {
	    public void run() {
		try {
		    Vars.videoPlayerProcess.waitFor();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		try {
		    Vars.frame.updateStatusText();
		} catch (SocketException e) {
		    e.printStackTrace();
		}
		currentFilename = "nothing";
	    }
	}.start();
    }
}
