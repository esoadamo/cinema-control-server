package aho.gms;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class HTTPHandler extends AbstractHandler {

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

	response.setContentType("text/html; charset=utf-8");
	response.addHeader("Access-Control-Allow-Origin", "*");
	response.setStatus(HttpServletResponse.SC_OK);

	final Client client = new Client(request.getRemoteAddr());
	if (client.exists())
	    client.updateLastSeen();

	final PrintWriter out = response.getWriter();

	if (Vars.disconnectClients.get()) {
	    out.println("quit");
	    baseRequest.setHandled(true);
	    return;
	}

	final DSMap params = new DSMap(request);

	if (params.keyExists("ping")) {
	    out.println("pong");
	} else if (params.keyExists("registerMe")) { // New client incoming
	    if (!client.exists()) {
		System.out.printf("%s connected%n", client.toString());
		Vars.listClients.wait4Unlock();
		Vars.listClients.setLocked(true);
		Vars.listClients.add(client);
		Vars.listClients.setLocked(false);
		Vars.frame.updateStatusText();
	    }
	    out.println("registered");
	} else if (params.keyExists("bye") && client.exists()) { // Client disconnecting
	    System.out.printf("%s disconnected%n", client.toString());
	    client.disconnect();
	    Vars.frame.updateStatusText();
	} else if (params.keyExists("list")) {
	    StringBuilder sb = new StringBuilder("d..;");
	    String pathAppend = params.getString("list");
	    if (Vars.isUnixLike())
		pathAppend = pathAppend.replaceAll("/", "/");
	    else
		pathAppend = pathAppend.replaceAll("/", "\\\\");
	    final File targetDir = new File(Vars.dirCinema.getAbsolutePath() + pathAppend);
	    System.out.printf("Listing directory %s%n", targetDir.getAbsolutePath());
	    for (final File f : targetDir.listFiles()) {
		if (f.isDirectory())
		    sb.append("d");
		else
		    sb.append("f");
		sb.append(f.getName());
		sb.append(";");
	    }
	    out.println(sb.toString());
	} else if (params.keyExists("play")) {
	    String pathAppend = params.getString("play");
	    if (Vars.isUnixLike())
		pathAppend = pathAppend.replaceAll("/", "/");
	    else
		pathAppend = pathAppend.replaceAll("/", "\\\\");
	    final File targetFile = new File(Vars.dirCinema.getAbsolutePath() + pathAppend);
	    System.out.printf("Playing file %s%n", targetFile.getAbsolutePath());
	    VideoPlayer.playFile(targetFile);
	} else if (params.keyExists("pause")) {
	    System.out.println("Pausing");
	    Vars.controls.pause(VideoPlayer.robot);
	} else if (params.keyExists("skip")) {
	    System.out.println("Skipping few seconds");
	    Vars.controls.forward(VideoPlayer.robot);
	} else if (params.keyExists("skip2")) {
	    System.out.println("Skipping few more seconds");
	    Vars.controls.forward2(VideoPlayer.robot);
	} else if (params.keyExists("prev")) {
	    System.out.println("Returning few seconds");
	    Vars.controls.backward(VideoPlayer.robot);
	} else if (params.keyExists("prev2")) {
	    System.out.println("Returning few more seconds");
	    Vars.controls.backward2(VideoPlayer.robot);
	} else if (params.keyExists("volU")) {
	    System.out.println("Adding volume");
	    Vars.controls.volumeUp(VideoPlayer.robot);
	} else if (params.keyExists("volD")) {
	    System.out.println("Decreasing volume");
	    Vars.controls.volumeDown(VideoPlayer.robot);
	} else if (params.keyExists("speedD")) {
	    System.out.println("Decreasing speed");
	    Vars.controls.speedDown(VideoPlayer.robot);
	} else if (params.keyExists("speedU")) {
	    System.out.println("Increasing speed");
	    Vars.controls.speedUp(VideoPlayer.robot);
	} else if (params.keyExists("audN")) {
	    System.out.println("Switching to next audio");
	    Vars.controls.nextAudio(VideoPlayer.robot);
	} else if (params.keyExists("audP")) {
	    System.out.println("Switching to prev audio");
	    Vars.controls.prevAudio(VideoPlayer.robot);
	} else if (params.keyExists("subS")) {
	    System.out.println("Switching subtitles");
	    Vars.controls.subtitlesSwitch(VideoPlayer.robot);
	} else if (params.keyExists("subN")) {
	    System.out.println("Switching to next subtitles");
	    Vars.controls.nextSubtitles(VideoPlayer.robot);
	} else if (params.keyExists("subN")) {
	    System.out.println("Switching to prev subtitles");
	    Vars.controls.prevSubtitles(VideoPlayer.robot);
	} else if (params.keyExists("fullscreen")) {
	    System.out.println("Changing fullscreen");
	    Vars.controls.toggleFullscreen(VideoPlayer.robot);
	} else if (params.keyExists("stop")) {
	    System.out.println("Stopping player");
	    VideoPlayer.kill();
	} else if (params.keyExists("adjustVideo")) {
	    System.out.println("Adjusting video format");
	    Vars.controls.adjustVideoFormat(VideoPlayer.robot);
	} else if (params.keyExists("whatIsPlaying")) {
	    out.println("playing" + VideoPlayer.currentFilename);
	}

	baseRequest.setHandled(true);
    }
}
