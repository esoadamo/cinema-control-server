package aho.gms;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.jetty.server.Server;

import aho.gms.maps.KeyMap;
import aho.gms.maps.OMXPlayer;
import aho.gms.maps.VLC;
import aho.util.LEArrayList;


public class Vars {
    // TODO Put this things inside config file
    public static int serverPort = 64198;
    public static String gameName = "Cinema";
    public static double version = 1.0;
    public static long keepPlayerOnlineFor = 5000; //How long keep player online after last packet was recivied
    
    public static Process videoPlayerProcess = null;
    
    public static final LEArrayList<Client> listClients = new LEArrayList<Client>();
    
    public static final FrameCinema frame = new FrameCinema();
    
    public static File dirCinema = null;
    static{
	if(isUnixLike())
	    dirCinema = new File("~/Videos/");
	else
	    dirCinema = new File(System.getenv("userprofile") + "\\Videos");
    }
    
    public static KeyMap controls = null;
    static{
	if(isUnixLike())
	    controls = new OMXPlayer();
	else
	    controls = new VLC();
    }

    public static Server server;

    public static AtomicBoolean disconnectClients = new AtomicBoolean(false);
    
    /**
     * Checks if system is Windows based or Unix like
     * 
     * @return true if system is Unix like, false if this system is Windows based
     */
    public static boolean isUnixLike() {
	return !System.getProperty("os.name").toUpperCase().contains("WIN");
    }
}
