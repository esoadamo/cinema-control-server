package aho.gms.maps;

import java.awt.Robot;

public interface KeyMap {
    public void pause(final Robot r);
    public void forward(final Robot r);
    public void  forward2(final Robot r);
    public void  backward(final Robot r);
    public void  backward2(final Robot r);
    public void  volumeUp(final Robot r);
    public void  volumeDown(final Robot r);
    public void  subtitlesSwitch(final Robot r);
    public void  nextSubtitles(final Robot r);
    public void  prevSubtitles(final Robot r);
    public void  nextAudio(final Robot r);
    public void  prevAudio(final Robot r);
    public void  speedUp(final Robot r);
    public void  speedDown(final Robot r);
    public void  	toggleFullscreen(final Robot r);
    public void adjustVideoFormat(final Robot r);
}
