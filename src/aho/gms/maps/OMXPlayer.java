package aho.gms.maps;

import java.awt.Robot;
import java.awt.event.KeyEvent;

public class OMXPlayer implements KeyMap {

    @Override
    public void pause(Robot r) {
	type(KeyEvent.VK_SPACE, r);
    }

    @Override
    public void forward(Robot r) {
	type(KeyEvent.VK_RIGHT, r);

    }

    @Override
    public void forward2(Robot r) {
	type(KeyEvent.VK_UP, r);
    }

    @Override
    public void backward(Robot r) {
	type(KeyEvent.VK_LEFT, r);

    }

    @Override
    public void backward2(Robot r) {
	type(KeyEvent.VK_DOWN, r);

    }

    @Override
    public void volumeUp(Robot r) {
	type(KeyEvent.VK_PLUS, r);

    }

    @Override
    public void volumeDown(Robot r) {
	type(KeyEvent.VK_MINUS, r);

    }

    @Override
    public void subtitlesSwitch(Robot r) {
	type(KeyEvent.VK_S, r);

    }

    @Override
    public void nextSubtitles(Robot r) {
	type(KeyEvent.VK_M, r);

    }

    @Override
    public void prevSubtitles(Robot r) {
	type(KeyEvent.VK_N, r);

    }

    @Override
    public void nextAudio(Robot r) {
	type(KeyEvent.VK_K, r);

    }

    @Override
    public void prevAudio(Robot r) {
	type(KeyEvent.VK_I, r);

    }

    @Override
    public void speedUp(Robot r) {
	type(KeyEvent.VK_1, r);

    }

    @Override
    public void speedDown(Robot r) {
	type(KeyEvent.VK_2, r);

    }

    private static void type(final int key, final Robot robot) {
	robot.keyPress(key);
	try {
	    Thread.sleep(100);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	robot.keyRelease(key);
    }

    @Override
    public void toggleFullscreen(Robot r) {
	//Not working in OMXPlayer
    }

    @Override
    public void adjustVideoFormat(Robot r) {
	//Not working in OMXPlayer
    }

}
