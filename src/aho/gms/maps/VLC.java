package aho.gms.maps;

import java.awt.Robot;
import java.awt.event.KeyEvent;

public class VLC implements KeyMap {

    @Override
    public void pause(Robot r) {
	type(KeyEvent.VK_SPACE, r);
    }

    @Override
    public void forward(Robot r) {
	r.keyPress(KeyEvent.VK_SHIFT);
	type(KeyEvent.VK_RIGHT, r);
	r.keyRelease(KeyEvent.VK_SHIFT);
    }

    @Override
    public void forward2(Robot r) {
	r.keyPress(KeyEvent.VK_CONTROL);
	type(KeyEvent.VK_RIGHT, r);
	r.keyRelease(KeyEvent.VK_CONTROL);
    }

    @Override
    public void backward(Robot r) {
	r.keyPress(KeyEvent.VK_SHIFT);
	type(KeyEvent.VK_LEFT, r);
	r.keyRelease(KeyEvent.VK_SHIFT);

    }

    @Override
    public void backward2(Robot r) {
	r.keyPress(KeyEvent.VK_CONTROL);
	type(KeyEvent.VK_LEFT, r);
	r.keyRelease(KeyEvent.VK_CONTROL);

    }

    @Override
    public void volumeUp(Robot r) {
	r.keyPress(KeyEvent.VK_CONTROL);
	type(KeyEvent.VK_UP, r);
	r.keyRelease(KeyEvent.VK_CONTROL);
    }

    @Override
    public void volumeDown(Robot r) {
	r.keyPress(KeyEvent.VK_CONTROL);
	type(KeyEvent.VK_DOWN, r);
	r.keyRelease(KeyEvent.VK_CONTROL);
    }

    @Override
    public void subtitlesSwitch(Robot r) {
	r.keyPress(KeyEvent.VK_SHIFT);
	type(KeyEvent.VK_V, r);
	r.keyRelease(KeyEvent.VK_SHIFT);
    }

    @Override
    public void nextSubtitles(Robot r) {
	type(KeyEvent.VK_V, r);

    }

    @Override
    public void prevSubtitles(Robot r) {
	type(KeyEvent.VK_V, r);

    }

    @Override
    public void nextAudio(Robot r) {
	type(KeyEvent.VK_B, r);

    }

    @Override
    public void prevAudio(Robot r) {
	type(KeyEvent.VK_B, r);

    }

    @Override
    public void speedUp(Robot r) {
	type(KeyEvent.VK_PLUS, r);
    }

    @Override
    public void speedDown(Robot r) {
	type(KeyEvent.VK_MINUS, r);

    }

    private static void type(final int key, final Robot robot) {
	robot.keyPress(key);
	try {
	    Thread.sleep(100);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	robot.keyRelease(key);
    }

    @Override
    public void toggleFullscreen(Robot r) {
	type(KeyEvent.VK_F, r);
    }

    @Override
    public void adjustVideoFormat(Robot r) {
	type(KeyEvent.VK_A, r);
    }

}
